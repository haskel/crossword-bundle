var crossword = {
    id:             0,
    isInited:       false,
    wordSelected:   0,
    crosswordWords: [],
    wordsIndex:     [],

    init: function() {
        var id = $("#crossword").data('id');
        if (parseInt(id) > 0) {
            this.id       = id;
            this.isInited = true;
        } else {
            return;
        }

        this.initIndexes();
        this.initWordSelection();
        this.initWordFilling();
    },

    initWordFilling: function() {
        var self = this;

        $('.crossword .letter-input').keyup(function(e) {
            self.removeMarkWrongWord(self.wordSelected);
            self.switchPosition(e);
            self.checkWord(e);
        });

        $('.crossword .letter-input').keydown(function(e) {
//        removeMarkWrongWord(wordSelected);
//        switchPosition(e);
//        checkCharCount(e);
        });
    },

    initWordSelection: function() {
        var self = this;

        $(".crossword .letter-input").focus(function(){
            var x       = $(this).data('x');
            var y       = $(this).data('y');
            var wordNum = self.wordsIndex[x + "-" + y][0];

            if ($("#letter-" + x + "-" + y).hasClass('word-selected')) {
                return;
            }

            self.selectWord(wordNum);
        });

        $(".crossword-questions .question").click(function(){
            var wordNum = $(this).data('number');
            self.selectWord(wordNum);
        });
    },

    initIndexes: function() {
        var self = this;

        $.ajax({
            type:     "GET",
            url:      '/crossword/' + self.id + '/words-coordinates',
            dataType: "json",

            success: function(response) {
                if (response.success) {
                    self.crosswordWords = response.coordinates;

                    $.each(self.crosswordWords, function (num, coords) {
                        console.log(coords);
                        console.log(num);
                        coords.forEach(function (coord) {
                            if (typeof self.wordsIndex[coord.x + '-' + coord.y] == 'undefined') {
                                self.wordsIndex[coord.x + '-' + coord.y] = [];
                            }
                            self.wordsIndex[coord.x + '-' + coord.y].push(num);
                        });
                    });
                }
            },

            error: function(response) {
                console.log('coordinates not loaded');
            }
        });


    },

    selectWord: function(wordNum) {
        this.wordSelected = wordNum;

        var coords  = this.crosswordWords[wordNum];
        $(".letter").removeClass('word-selected');
        coords.forEach(function (coord) {
            $("#letter-" + coord.x + "-" + coord.y).addClass('word-selected');
        });
    },

    markRightWord: function(wordNum) {
        var coords  = this.crosswordWords[wordNum];
        coords.forEach(function (coord) {
            $("#letter-input-" + coord.x + "-" + coord.y).removeClass('word-wrong');
            $("#letter-input-" + coord.x + "-" + coord.y).addClass('word-right');
        });
    },

    markWrongWord: function(wordNum) {
        var coords  = this.crosswordWords[wordNum];
        coords.forEach(function (coord) {
            $("#letter-input-" + coord.x + "-" + coord.y).removeClass('word-right');
            $("#letter-input-" + coord.x + "-" + coord.y).addClass('word-wrong');
        });
    },

    removeMarkWrongWord: function(wordNum) {
        var coords  = this.crosswordWords[wordNum];
        coords.forEach(function (coord) {
            $("#letter-input-" + coord.x + "-" + coord.y).removeClass('word-wrong');
        });
    },


    checkWord: function(e) {
        if (e.which < 32) {
            return;
        }

        var x = $("#" + e.target.id).data('x');
        var y = $("#" + e.target.id).data('y');
        var coords = this.crosswordWords[this.wordSelected];
        var word = "";
        var lastChar = coords[coords.length - 1];

        if (lastChar.x != x || lastChar.y != y) {
            return;
        }

        coords.forEach(function (coord) {
            var char = $("#letter-input-" + coord.x + "-" + coord.y).val();
            word += char.length ? char : " ";
        });

        self = this;
        $.ajax({
            type:     "POST",
            url:      '/crossword/' + self.id + '/check-word',
            data:     {
                questionId: self.wordSelected,
                word:       word
            },
            dataType: "json",

            success: function(response) {
                if (response.success) {
                    self.markRightWord(self.wordSelected);
                } else {
                    self.markWrongWord(self.wordSelected);
                }
            },

            error: function(response) {
                self.markWrongWord(self.wordSelected);
            }
        });

    },

    switchPosition: function(e) {
        if (e.which < 32) {
            return;
        }

        var x              = $("#" + e.target.id).data('x');
        var y              = $("#" + e.target.id).data('y');
        var coords         = this.crosswordWords[this.wordSelected];
        var isCurrentCoord = false;

        coords.forEach(function (coord) {
            if (coord.x == x && coord.y == y) {
                isCurrentCoord = true;
                return;
            }

            if (isCurrentCoord) {
                $("#letter-input-" + coord.x + "-" + coord.y).focus();
                isCurrentCoord = false;
            }
        });
    }
};