<?php
namespace SC\CrosswordBundle\Controller;

use SC\ArticleBundle\Entity\Article;
use SC\ArticleBundle\Entity\ArticleContent;
use SC\ArticleBundle\Entity\ArticleFlatContent;
use SC\ArticleBundle\Entity\ArticleMeta;
use SC\ArticleBundle\Entity\ArticleStats;
use SC\CrosswordBundle\Entity\Crossword;
use SC\CrosswordBundle\Entity\Letter;
use SC\CrosswordBundle\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AdminController extends Controller
{

    /**
     * @param Request $request
     *
     * @Template
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $crosswords = $this->getDoctrine()
                           ->getRepository('CrosswordBundle:Crossword')
                           ->findBy([], ['id' => 'desc']);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $crosswords,
            $request->query->get('page', 1),
            $limit = 20
        );

        return [
            'pagination' => $pagination
        ];
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $crossword = new Crossword();
        $crossword->setName($request->get('name'));
        $crossword->setWidth($request->get('width'));
        $crossword->setHeight($request->get('height'));

        $validator = $this->get('validator');
        $errors    = $validator->validate($crossword);

        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            return new JsonResponse([
                'success' => false,
                'errors'  => $errors,
            ]);
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->persist($crossword);
        $em->flush($crossword);

        $row = $this->renderView('@Crossword/Admin/row.html.twig', ['crossword' => $crossword]);

        return new JsonResponse([
            'success' => true,
            'row'     => $row
        ]);
    }

    /**
     * @param Crossword $crossword
     *
     * @Template
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return array
     */
    public function editAction(Crossword $crossword)
    {
        return [
            'crossword' => $crossword
        ];
    }

    /**
     * @param Request   $request
     * @param Crossword $crossword
     *
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return Response
     */
    public function addLetterAction(Crossword $crossword, Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $char = $request->get('letter');
        $x    = $request->get('x');
        $y    = $request->get('y');

        if (strlen($char) == 0) {
            $letter = $em->createQueryBuilder()
                         ->select('letter')
                         ->from('CrosswordBundle:Letter', 'letter')
                         ->join('letter.crossword', 'crossword')
                         ->where('letter.x = :x and letter.y = :y')
                         ->andWhere('crossword.id = :crosswordId')
                         ->setParameter('x', $x)
                         ->setParameter('y', $y)
                         ->setParameter('crosswordId', $crossword->getId())
                         ->getQuery()
                         ->getOneOrNullResult();

            if ($letter) {
                $em->remove($letter);
                $em->flush($letter);
            }

            return new JsonResponse(['success' => true]);
        }

        $letter = new Letter();
        $letter->setLetter(str_replace("ё", "е", strtolower($char)));
        $letter->setX($x);
        $letter->setY($y);
        $letter->setCrossword($crossword);

        $validator = $this->get('validator');
        $errors    = $validator->validate($letter);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            return new JsonResponse([
                                        'success' => false,
                                        'errors'  => $errors,
                                    ]);
        }

        $em->persist($letter);
        $em->flush($letter);

        return new JsonResponse(['success' => true]);
    }

    /**
     * @param Crossword $crossword
     *
     * @Template
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return array
     */
    public function questionsAction(Crossword $crossword)
    {

        $crossword->getWords();

        return ['crossword' => $crossword];
    }

    /**
     * @param Crossword $crossword
     *
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return array
     */
    public function questionsSaveAction(Crossword $crossword, Request $request)
    {
        /** @var Question $question */
        $em = $this->get('doctrine.orm.default_entity_manager');

        foreach ($request->get('questions', []) as $questionRaw) {
            $question = $em->createQueryBuilder()
                           ->select('question')
                           ->from('CrosswordBundle:Question', 'question')
                           ->join('question.crossword', 'crossword')
                           ->where('question.number = :number')
                           ->andWhere('crossword.id = :crosswordId')
                           ->setParameter('number', $questionRaw['number'])
                           ->setParameter('crosswordId', $crossword->getId())
                           ->getQuery()
                           ->getOneOrNullResult();

            if ($question) {
                $question->setText($questionRaw['text']);
                $em->persist($question);
            } else {
                $question = new Question();
                $question->setText($questionRaw['text']);
                $question->setNumber($questionRaw['number']);
                $question->setCrossword($crossword);
                $em->persist($question);
            }
        }

        $em->flush();

        return new JsonResponse(['success' => true]);
    }


    /**
     * @param Crossword $crossword
     *
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return array
     */
    public function createArticleAction(Crossword $crossword, Request $request)
    {
        if ($this->get('security.context')->isGranted('ROLE_EDITOR_IN_CHIEF') ||
            $this->get('security.context')->isGranted('ROLE_ADMIN')
        ) {

        }
        $article = new Article();
        $article->setAuthorId($this->get('security.context')->getToken()->getUser()->getId());
        $article->setAuthor($this->get('security.context')->getToken()->getUser());
        $article->setCreatedAt(new \DateTime());
        $article->setPublishedAt(new \DateTime());
        $article->setCommentOutPutStyle(1);
        $article->setIsPublished(false);
        $article->setTitle(sprintf('Кроссворд "%s"', $crossword->getName()));
        $article->setShortAnounce($request->get('announce'));
        $article->setLongAnounce($request->get('longAnnounce'));
        $article->setContent((new ArticleContent())->setArticle($article));
        $article->setFlatContent((new ArticleFlatContent())->setArticle($article));

        $articleMeta = new ArticleMeta();
        $article->setMeta($articleMeta);

        $article->setViewCnt((new ArticleStats())->setCnt(0)->setArticle($article));


        $article->setPath('');

        $this->container->get('article.service')->save($article);
    }
}
